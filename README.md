# Basic docker setup for trainig app

Contains 4 containers: nginx, php (v7.3, with some tools like composer, php-cs-fixer, xdebug), db (MySQL 5.7), phpmyadmin (available on :8080 port)

Quick start:

- docker-compose up
- If you want - login into asper-db container (docker exec -it asper-db /bin/bash) and add database and user for application. Default database root password is ```supersecurepassword```
- Place app code in /src directory
- configure app .env (see readme in asper-app)
- go to http://asper.local
